@extends('layouts.main')

@section('container')

<div class="container-fluid">
    <h1 class="mt-4">Tabel User</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Tabel User</li>
        </ol>
    <a href="/tambah" type="button" class="btn btn-success">Tambah Data</a>
    <table class="table mt-2">
      <thead class="table-dark">
        <tr>
            <td>Nama</td>
            <td>Email</td>
            <td>No. Tlp</td>
            <td>Alamat</td>
            <td>Action</td>
        </tr>
      </thead>
      <tbody>
        @foreach($users as $user)
        <tr>
            <td> {{ $user->name }} </td>
            <td> {{ $user->email }} </td>
            <td> {{ $user->noTlp }} </td>
            <td> {{ $user->alamat }} </td>
            <td>
                <a href="/edit-data/{{ $user->id }}/edit" class="btn btn-warning">Edit</a>
                <!-- <a href="" class="btn btn-danger">Delete</a> -->
                <form action="/delete/{{ $user->id }}" method="post" class="d-inline">
                      @method('delete')
                      @csrf
                      <button class="btn btn-danger"
                      onclick="return confirm('Are you sure?')">Delete</button>
                </form>
            </td>
            </tr>
        @endforeach
      </tbody>
    </table>
</div>

@endsection